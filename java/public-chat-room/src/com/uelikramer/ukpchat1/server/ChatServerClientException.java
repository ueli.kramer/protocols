/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uelikramer.ukpchat1.server;

import java.util.HashMap;

/**
 *
 * @author kramer
 */
public class ChatServerClientException extends Exception {
    
    ChatServerClient serverClient;
    int code;

    public ChatServerClientException(ChatServerClient client, int code) {
        serverClient = client;
        this.code = code;
    }
    
    public HashMap<String, String> getParameters() {
        HashMap<String, String> parameters = new HashMap<>();
        parameters.put("code", "" + this.code);
        return parameters;
    }
    
}