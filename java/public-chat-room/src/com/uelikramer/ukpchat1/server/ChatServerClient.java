/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uelikramer.ukpchat1.server;

import java.io.IOException;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.HashMap;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kramer
 */
public class ChatServerClient implements Runnable {
    private Socket socket;
    private Scanner in;
    private PrintWriter out;
    private ChatServer server;
    
    private String username;

    public ChatServerClient(ChatServer server, Socket s) {
        socket = s;
        this.server = server;
    }

    @Override
    public void run() {
        try {
            in = new Scanner(socket.getInputStream());
            out = new PrintWriter(socket.getOutputStream());

            while (true) {
                String frameRaw = "";
                while (true) {
                    if (in.hasNext()) {
                        String input = in.nextLine();
                        if (!"".equals(frameRaw)) {
                            frameRaw += System.getProperty("line.separator");
                        }
                        frameRaw += input;
                        if ("END".equals(input)) {
                            break;
                        }
                    }
                }
                
                try {
                    ChatServerFrame frame = ChatServerProtocol.parseRawToFrame(this, frameRaw);
                    handleRequest(frame);
                } catch (ChatServerClientException ex) {
                    sendCommand("CORE", ex.getParameters(), 'e');
                }
            }

        } catch (IOException ex) {
            Logger.getLogger(ChatServerClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void sendCommand(String command) {
        sendCommand(command, new HashMap<String, String>());
    }
    
    public void sendCommand(String command, HashMap<String, String> parameters) {
        sendCommand(command, parameters, 'r');
    }
    
    public void sendCommand(String command, HashMap<String, String> parameters, char type) {
        ChatServerFrame frame;
        try {
            frame = ChatServerProtocol.buildFrame(command, parameters, type);
            out.println(frame.getRaw());
            out.flush();
        } catch (ChatServerProtocolException ex) {
            Logger.getLogger(ChatServerClient.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    private void handleRequest(ChatServerFrame frame) {
        switch(frame.getCommand()) {
            case "CONNECT":
                // add client to our chat clients
                server.addChatClient(this);
                break;
            case "LOGIN":
                username = frame.getParameter("login");
                server.loginUser(this);
                break;
            case "LOGOUT":
                server.logoutUser(this);
                break;
            case "SEND":
                server.sendMessage(this, frame.getParameter("message"));
                break;
        }
    }
    
    public String getUsername() {
        return username;
    }
}
