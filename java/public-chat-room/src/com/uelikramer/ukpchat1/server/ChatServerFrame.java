/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.uelikramer.ukpchat1.server;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

/**
 *
 * @author kramer
 */
public class ChatServerFrame {
    private String protocol;
    private String command;
    private HashMap<String, String> parameters;
    private String end;
    private char type;

    public String getProtocol() {
        return protocol;
    }

    public void setProtocol(String protocol) {
        this.protocol = protocol;
    }

    public char getType() {
        return type;
    }

    public void setType(char type) {
        this.type = type;
    }

    public String getCommand() {
        return command;
    }

    public void setCommand(String command) {
        this.command = command;
    }

    public HashMap<String, String> getParameters() {
        return this.parameters;
    }
    
    public String getParameter(String key) {
        return parameters.get(key);
    }

    public void addParameter(String key, String value) {
        this.parameters.put(key, value);
    }
    
    public void setParameters(HashMap<String, String> parameters) {
        this.parameters = parameters;
    }

    public String getEnd() {
        return end;
    }

    public void setEnd(String end) {
        this.end = end;
    }
    
    public ChatServerFrame() {
        
    }

    public ChatServerFrame(String protocol, char type, String command, HashMap<String, String> parameters, String end) {
        this.protocol = protocol;
        this.type = type;
        this.command = command;
        this.parameters = parameters;
        this.end = end;
    }
    
    public String getContent() {
        Iterator it = parameters.entrySet().iterator();
        String content = "";
        while (it.hasNext()) {
            Map.Entry parameter = (Map.Entry)it.next();
            content += parameter.getKey() + ": " + parameter.getValue() + System.getProperty("line.separator");
            it.remove();
        }
        return content;
    }
    
    public String getRaw() {
        return buildHeader() + getContent() + buildEnd();
    }
    
    private String buildHeader() {
        return protocol + ":" + type + ":" + command + System.getProperty("line.separator");
    }
    
    private String buildEnd() {
        return System.getProperty("line.separator") + end;
    }
}
