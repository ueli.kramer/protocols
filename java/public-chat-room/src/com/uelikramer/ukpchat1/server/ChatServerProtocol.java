/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uelikramer.ukpchat1.server;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

abstract public class ChatServerProtocol {

    static private final String protocol = "ukpchat1";
    static private final char[] types = {'r', 'a', 'e'};
    static private final String[] receiveCommands = {"CONNECT", "LOGIN", "SEND", "LOGOUT"};
    static private final String[] sendCommands = {"LOGIN", "LOGOUT", "MESSAGE", "USERS", "CORE"};

    static public ChatServerFrame parseRawToFrame(ChatServerClient client, String raw) throws ChatServerClientException {
        String[] lines = raw.split(System.getProperty("line.separator"));
        HashMap<String, String> parameters = new HashMap<>();

        ChatServerFrame frame;
        frame = new ChatServerFrame();
        
        for (int i = 0; i < lines.length; i++) {
            if (i == 0) {
                parseHeader(frame, client, lines[i]);
                continue;
            }
            if (i == lines.length - 1) {
                parseEnd(frame, client, lines[i]);
                continue;
            }
            parseParameterLine(frame, client, lines[i]);
            // @todo: parse all other lines and put them into parameters
        }
        return frame;
    }
    
    static public ChatServerFrame buildFrame(String command, HashMap<String, String> parameters, char type) throws ChatServerProtocolException {
        if (new String(types).indexOf(type) == -1) {
            throw new ChatServerProtocolException("Type " + type + " not defined!");
        }
        
        ChatServerFrame frame = new ChatServerFrame();
        frame.setProtocol(protocol);
        frame.setType(type);
        frame.setParameters(parameters);
        frame.setCommand(command);
        frame.setEnd("END");
        
        return frame;
    }
    
    static private String buildHeader(String message, char type) {
        return protocol + ":" + type + ":" + message + System.getProperty("line.separator");
    }
    
    static private String buildEnd() {
        return System.getProperty("line.separator") + "END";
    }
    
    static private void parseHeader(ChatServerFrame frame, ChatServerClient client, String line) throws ChatServerClientException {
        String[] header = line.split(":");
        frame.setProtocol(header[0]);
        frame.setType(header[1].toCharArray()[0]);
        frame.setCommand(header[2]);
        if (!frame.getProtocol().equals(protocol) || header.length != 3) {
            // write error 902
            throw new ChatServerClientException(client, 902);
        }
        if (new String(types).indexOf(frame.getType()) == -1) {
            // write error 904
            throw new ChatServerClientException(client, 904);
        }
        if (!Arrays.asList(receiveCommands).contains(frame.getCommand())) {
            // write error 901
            throw new ChatServerClientException(client, 901);
        }
    }
    
    static private void parseParameterLine(ChatServerFrame frame, ChatServerClient client, String line) {
        String[] parameter = line.split(": ", 1);
        if (parameter.length < 2) {
            return;
        }
        
    }
    
    static private void parseEnd(ChatServerFrame frame, ChatServerClient client, String line) throws ChatServerClientException {
        frame.setEnd(line);
        if (!"END".equals(line)) {
            // write error 903
            throw new ChatServerClientException(client, 903);
        }
    }
}
