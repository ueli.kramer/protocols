/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.uelikramer.ukpchat1.server;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Vector;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author kramer
 */
public class ChatServer {
    private Vector<ChatServerClient> chatClients = new Vector<>();
    private Vector<ChatServerClient> loggedInUsers = new Vector<>();
    private ServerSocket server;

    public ChatServer() throws IOException {
        final int PORT = 31337;
        System.out.println("Server is up and running!");
        server = new ServerSocket(PORT);

        while (true) {
            Socket s = server.accept();
            ChatServerClient client = new ChatServerClient(this, s);
            Thread t = new Thread(client);
            t.start();
        }
    }
    
    public void addChatClient(ChatServerClient client) {
        chatClients.add(client);
        System.out.println("Client #" + chatClients.size() + " connected!");
        client.sendCommand("LOGIN");
    }
    
    public void loginUser(ChatServerClient client) {
        if (!chatClients.contains(client)) {
            // @todo: print error
            return;
        }
        if (loggedInUsers.contains(client)) {
            // @todo: print error, er003
            return;
        }
        int index = chatClients.indexOf(client);
        loggedInUsers.add(client);
        // @todo: check whether the username is already in use, er001
        System.out.println("Client #" + index + " logged in as " + client.getUsername());
        // @todo: send answer "LOGIN" and "USERS" (also to other users)
    }
    
    public void logoutUser(ChatServerClient client) {
        if (!loggedInUsers.contains(client)) {
            // @todo: print error, er101
            return;
        }
        loggedInUsers.remove(client);
        // @todo: send answer "LOGOUT" and "USERS" (also to other users)
    }
    
    public void sendMessage(ChatServerClient client, String message) {
        if (!loggedInUsers.contains(client)) {
            // @todo: print error, er101
            return;
        }
        message = message.trim();
        if (message.isEmpty()) {
            // @todo: print error, er201
            return;
        }
        // @todo: send answer and send message to all other clients
    }

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        try {
            new ChatServer();
        } catch (IOException ex) {
            Logger.getLogger(ChatServer.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

}
