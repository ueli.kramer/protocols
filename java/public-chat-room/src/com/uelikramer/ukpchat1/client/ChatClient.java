package com.uelikramer.ukpchat1.client;

import java.io.IOException;
import java.io.PrintStream;
import java.net.Socket;
import java.util.Scanner;

/**
 *
 * @author kramer
 */
public class ChatClient {
    
    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) throws IOException {
        new ChatClient();
    }
    
    private PrintStream out;
    private Scanner in;

    public ChatClient() throws IOException {
        Socket socket = new Socket("127.0.0.1", 31337);
        in = new Scanner(socket.getInputStream());
        out = new PrintStream(socket.getOutputStream());
        connect();
        while (true) {
            if (in.hasNext()) {
                String input = in.nextLine();
                System.out.println(input);
            }
        }
    }
    
    private void connect() {
        out.println("ukpchat1:a:CONNECT");
        out.println("");
        out.println("END");
        out.flush();
    }
    
}
